
/*

create TODO/"My Task List" application

1) At the start - page is empty body [so all elements should be programmatically created]

2) Application should serve a list of tasks: show, add, remove, [advanced] edit

3) Each task contains: name [required], description [optional], datetime [datetime when task was added]

4) Task list should persist in browser's localStorage so survive page reloads


 */
