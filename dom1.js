
var a;

a = document.getElementById('test');

document.addEventListener('DOMContentLoaded', function() {
    var b;

    b = document.getElementById('test');
});

// document.getElementById
// document.getElementsByName
// document.getElementsByClassName
// document.querySelector
// document.querySelectorAll

// elem.matches(css)
// elem.closest()

// https://learn.javascript.ru/traversing-dom

// elem.innerHTML
// elem.data
// elem.style.

// elem.className  elem.classList
// elem.hasAttribute(name)
// elem.getAttribute(name)
// elem.setAttribute(name, value)
// elem.removeAttribute(name)
// a.href; img.src

// elem.children
// elem.appendChild
// elem.insertBefore(elem, nextSibling)
// elem.removeChild
// elem.replaceChild
// elem.remove
