
var a, b, c, d;

// add watches in debugger

a = new Object();
a = {};

a.name = 'sergey';

// copy by link / by value
b = a;
b.class = 'high';

b = {
    name: 'alex',
    'age': 17
};

alert(b.name + ' ' + b.age);

// manipulating properties, checking
delete(b.name);
if ('age' in b) {
    c = 'name' in b;
    console.log(c);
}

// array-like access
console.log(b['age']);
c = 'age';
console.log(b[c]);
console.log(b.c);

c = {
    'name': 'hello',
    's123': 213
};

for (d in c) {
    console.log(d + ': ' + c[d]);
}

// properties, methods:
// string: .length, .indexOf()
// array: .length, .indexOf(), .pop(), .push(), .shift(), .unshift(), .slice()
