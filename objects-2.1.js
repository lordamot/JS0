
var obj, a, b, c, d;

obj = {
    someField: 'someValue',

    someMethod: function() {
        console.log('Some method called: ' + this.someField);
    }
};

// explicit binding

obj.someMethod();

// whoops we lost 'this'

a = obj.someMethod;
a();

window.someField = 'I am a value in window!';
a();

// restore it

b = a.bind(obj); // other function actually
b();

// common way to shoot in a leg

setTimeout(obj.someMethod, 100);// window.setTimeout

// proper way to not shoot yourself

setTimeout(obj.someMethod.bind(obj), 100);

// objects are Really flexible

c = function() {
    console.log('Some supercustom method from nowhere: ' + this.someField);
};

obj.superMethod = c;

obj.superMethod();

// in prepare for prototypes...

d = [1, 2, 3];
console.log(d.superSumX2());// error - no such method in array

Array.prototype.superSumX2 = function() { return 777; };
console.log(d.superSumX2());// hello
