
1. JS Overview. Gitlab

2. JS Functions part1 - basics
   JS debugger - basics
   alert prompt confirm console.log
   
   Task1
       "guess the number" game

3. Js Objects part1
   as assoc arrays
   properties, manipulating
   
   Task2
       isEmpty, sumAll, findMax
       arrays, array methods
   
4. JS Functions and Objects part2
       local scope, closures
       this, methods
       --
       recursion fail 20979 for me
       question about "how to secure from dev tools"
       
   Task3
       timers
    

5. CSS, DOM, docready

   Task4
       semaphore, html-based
       
6. JSON, localStorage

   Task5.1
       todo app [localstorage]

7. Prototypes p.2 - object constructors

   Task5.2
       todo app [localstorage]

8. Check results
   Explain backend for networking demo
   Networking p.1

9. Networking p.2
       file submit example

   Task 6
       todo app [through network]

10. Promises

11. Other modern language features


--------------------------------------
drafts below

    Task 7
           photogallery app [through network]

12.    TypeScript
13.    RxJS
14-16. Angular introduction
