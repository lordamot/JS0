
var a, key;

// localStorage.setItem('test', 'test me');

// console.log(localStorage.getItem('test'));

a = {test: 'test'};

localStorage.setItem('me', a);// does not work

for (a = 0; a < localStorage.length; a++) {
    key = localStorage.key(a);
    console.log('Key: ' + key + ' Value: ' + localStorage.getItem(key));
}

// localStorage.clear()

// sessionStorage - like localStorage but for one browser session only


a = {test: 'test'};
a = JSON.stringify(a);// to string
localStorage.setItem('test', a);
a = localStorage.getItem('test');// string
a = JSON.parse(a); // identical to source object
