
var a, b, c, d;

// global object - window

console.log(this);

a = 'I am a string';

// same var below

console.log(a);
console.log(this.a);
console.log(window.a);

// function local vars

function showMeLocal() {
    var a;

    a = 123;

    console.log(a);
    console.log(this.a);
    console.log(window.a);
}

showMeLocal();

// inner local scopes

function showMeLocalTwice() {
    var a;

    a = 456;

    function showMeLocalInner() {
        var a;

        a = 789;

        console.log(a);
        console.log(this.a);
        console.log(window.a);
    }

    showMeLocalInner();
}

showMeLocalTwice();
showMeLocalInner();// error - undefined is not a function

//closures

function makeMeClosure(someValue) {
    console.log(a);

    return function(otherArg) {
        //LexicalEnvironment: {...}
        return otherArg + someValue;
    }
}

b = makeMeClosure('let\'s see how that goes');
c = makeMeClosure('I am another function actually');

console.log(b('fn B: '));
console.log(c('fn C: '));

