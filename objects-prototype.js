
var a, b;

// prototype inheritance

a = {
    oldone: true,
    legacy: 666,
    method: function() {
        this.legacy = 777;
    }
};

b = {
    newone: true,
    oldone: false
};

b.__proto__ = a;

console.log(b.legacy);
console.log(b.newone);
console.log(b.oldone);

b.hasOwnProperty('legacy');

// (for key in obj)
// b.method(); ?

// object creation

function MakeMeMad(name) {
    this.name = name;
}

// prototype == { constructor: MakeMeMad }

MakeMeMad.prototype.getName = function() {
    console.log(this.name);
};

a = new MakeMeMad();
a.name;
a.getName();

// standard objects and "objects"

a = {};
a.__proto__;

a = 123;
a.__proto__; // Number.prototype

// functional-style prototype + private vars

function MakeMeMadTwice(name) {
    var a = Math.random();

    this.sayName = function() {
        console.log(name);
    };

    this.sayA = function() {
        console.log(a);
    };
}

a = MakeMeMadTwice('Twisted bastards!');
a.sayName();
a.sayA();

b = a.sayName;
b();

// prototype inheritance once more:   b.__proto__ = a.prototype

// override
b.prototype.run = function () {
    a.prototype.run.apply(this, arguments);
};
