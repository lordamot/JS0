
/*

create UI and script for controlling it

1) At the start - page is empty body [so all elements should be programmatically created]

2) Add 1 element to the page - square DIV 100x100px with some visible background color

3) UI contains 3 elements:

    a) text input + "set background" button

        this allows to input another color and after button click - set it as a new background color for that div

    b) "left" and "right" buttons

        these should move div to the left and to the right by 10-20px for each click

4) [ADVANCED] Add one more element to the UI - "add" button

        this one creates new same-styled div 20px right from last one for each click
        as a result, all created divs should be controlled from existing UI
        (for example, I pressed "add" 3 times - so have 4 divs in total; after that I am changing background style - should
        be changed for all divs at once)


 */
