
var a, b, c;

function isEmpty(obj) {
    // ???
}

function objSum(obj) {
    // ???
}

function objMax(obj) {
    // ???
}

a = {};
b = {
    me: 'you'
};
c = {
    John: 20,
    Mike: 40,
    Sam: 10
};

console.log(isEmpty(a)); // true
console.log(isEmpty(b)); // false
console.log(objSum(c)); // 70
console.log(objMax(c)); // Mike: 40

/*

  Arrays: write a program
  1) ask for elements count
  2) input all elements
  3) show min, max and sum of values

 */
