
// let, const

console.log(a);
//console.log(b);

var a = 123;
let b = 'name';
const c = document.getElementById('test');

// arrow functions, string templates

const myFunc = name => `Hi, ${name}`;

console.log(myFunc('Sergey'));


const myArr = [1, 2, 3, null];

const arr2 = myArr.map(item => !!item);
console.log(arr2);

// arrow funcs does not have this, arguments, super, or new.target.

// default function args

const meDefault = (name, age = 22) => {
    console.log(`Name is ${name} and age is ${age}`);
};

meDefault('Sergey');

// destructuring

const meObj = {
    name: 'name',
    age: 'age',
    test: 'best'
};

let {name, age, test} = meObj;

console.log(test);

let {x, y, name:z} = meObj;

console.log(x);
console.log(z);

let myArr1 = [0, 1, 2];
let [ma1, ma2, ma3] = myArr1;

// rest/all args

const meRest = (a, b, ...rest) => {
    console.log(rest);
};

meRest(1, 2, 3, 4, 5);

const meAll = (...all) => {
    console.log(meAll);
};

// classes!

class myClass {
    constructor(name) {
        this.name = name;
    }
};

class myAncestor extends myClass {
    printName() {
        console.log(this.name);
    }
};

const obj = new myAncestor('Mikhail');
obj.printName();

//http://es6-features.org/#Constants
//https://kangax.github.io/compat-table/es6/



