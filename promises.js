
var state, promiseOne, promiseTwo, promiseThree, p1, p2, p3;

state = 0;

function jobOne() {
    console.log('I am one');
    state = 1;
}

function jobTwo() {
    state = state * 2;
    console.log('I am job 2');
}

function jobThree() {
    state = state + 3;
    console.log('I am third job');
}


// sync

/*console.log('sync');
jobOne();
jobTwo();
jobThree();
console.log('state: ' + state);//*/


// not sync
/*console.log('not sync');
setTimeout(function() {
    jobOne();
}, 500);
setTimeout(function() {
    jobTwo();
}, 1000);
setTimeout(function() {
    jobThree();
}, 100);
console.log('state: ' + state);

// even worse:
setTimeout(function() {
    console.log('Lets wait a bit until everything is done');
    console.log('state: ' + state);
}, 3000);

//*/


// async, correct
/*console.log('not sync');
setTimeout(function() {
    jobOne();
    setTimeout(function() {
        jobTwo();
        setTimeout(function() {
            jobThree();
            console.log('state: ' + state);
        }, 100);
    }, 1000);
}, 500);
//*/


// async, more readable

/*function asyncOne() {
    jobOne();
    setTimeout(asyncTwo, 1000);
}

function asyncTwo() {
    jobTwo();
    setTimeout(asyncThree, 100);
}

function asyncThree() {
    jobThree();
    console.log('state: ' + state);
}

setTimeout(asyncOne, 500);//*/



//Promise

/*promiseOne = function() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            jobOne();
            resolve(state);
        }, 500);
    });
};

promiseTwo = function() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            jobTwo();
            resolve(state);
        }, 1000);
    });
};

promiseThree = function() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            jobThree();
            resolve(state);
        }, 100);
    });
};

promiseOne()
    .then(promiseTwo)
    .then(promiseThree)
    .then(function() {
        console.log('state: ', state);
    });//*/



//XMLHttpRequest as a promise

/*function httpGet(url) {

    return new Promise(function(resolve, reject) {

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status == 200) {
                resolve(this.response);
            } else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error("Network Error"));
        };

        xhr.send();
    });

};

httpGet("/some/url")
    .then(function(response) {
        console.log('Response: ');
        console.log(response);
    });//*/

// Promises API

//.then(resolve, reject)   .then(resolve/reject)
//.resolve  .reject

//.all

/*p1 = Promise.resolve(3);
p2 = 42;
p3 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3]).then(function(values) {
    console.log(values);
});//*/

//.race
//.catch
//.finally
