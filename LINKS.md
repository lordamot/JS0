
Useful links

http://htmlbook.ru/ html справочник
https://webref.ru/  обновленная версия справочника

https://learn.javascript.ru/getting-started current course

https://htmlacademy.ru/

https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference mozilla js reference

https://github.com/azat-io/you-dont-know-js-ru Серия книг "Вы не знаете JS". Бесплатны для прочтения онлайн.  
https://www.piter.com/collection/all/product/es6-i-ne-tolko